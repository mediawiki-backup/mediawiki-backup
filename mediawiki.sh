#!/bin/bash

source "$(dirname $0)/color"
source "$(dirname $0)/config"
source "$(dirname $0)/mediawikilib.sh"

function test_env {
  # The following packages are required
  if [ $(dpkg -l | grep mysql-client | grep -c ^ii) -eq 1 ]
    then
      echo 'The package mysql-client is missing, please install it!'
  fi
}

function select_mode {
  case "$@" in
    b)   
        backup fast
    ;;
    ba)   
        backup all
    ;;
    r)
        restore fast
    ;;
    ra)
        restore all
    ;;
    u)
        update
    ;;
    *) echo "Usuage: mediawiki-update.sh b=BACKUP|ba=BACKUP-ALL|r=RESTORE|ra=RESTORE-ALL"
    ;;
  esac
}

function backup {
  test_env
  ask_info backup
  set_env
  get_wiki_config
  do_mysql backup
  files backup
  archive create
  cleanall
}

function restore {
  test_env
  ask_info restore
  set_env
  archive extract
  files export
  get_wiki_config
  do_mysql restore
  cleanall
}


# Launch the program

echo -e "\n - - - - - - - - - - - - - -\n"
select_mode $@
echo " "
