#!/bin/bash

################################
# Utility and basic functions  #
################################

function ask_info {
  case $1 in
    backup)
      echo -e "What is your wiki path? (eg. /var/www/mysite.eu/mediawiki/w)"
      read -p " > "  WPATH
      echo -e "On which file do you want to save everything? (eg. /var/www/backup)"
      read -p " > " BPATH
      echo 
      echo -e "Set the archive name: (eg. mysite)"
      read -p " > " ARCH
    ;;
    restore)
      echo -e "Which file do you want to unzip? (eg. /var/www/mybackup.tar.gz)"
      read -p " > " ARCH
      echo -e "Where will be your web root directory? (eg. /var/www/mysite.com/wiki)"
      read -p " > " WPATH
      BPATH=$(dirname ${ARCH})
      ARCH=$(basename ${ARCH})
    ;;
  esac
}

function confirm ()
{
  for II in "$@"
  do
    VALUE=${!II}
    echo "$II = $VALUE"
  done
  read -p "Is that correct? (y/n) ->  " yn
  if [ $yn == n ]; then
    exit
  fi
}

function get_wiki_config {
  DB_NAME=$(grep ^\$wgDBname ${WPATH}/LocalSettings.php | grep -o -P '(?<=").*(?=")')
  DB_USER=$(grep ^\$wgDBuser ${WPATH}/LocalSettings.php | grep -o -P '(?<=").*(?=")')
  DB_PW=$(grep ^\$wgDBpassword ${WPATH}/LocalSettings.php | grep -o -P '(?<=").*(?=")')
  confirm "DB_NAME" "DB_USER" "DB_PW"
}

function set_env () {
  mkdir -p ${BPATH}/tmp
  if [ "$?" -ne 0 ]; then echo "command failedi, cannot create ${BPATH}/tmp"; exit 1; fi
  TMP=${BPATH}/tmp
  timestamp=$(date +%Y%m%d-%H%M%S)
}

function do_mysql {
  if [ $1 == "backup" ]; then
    mysqldump --user=$DB_USER --password=$DB_PW --databases $DB_NAME > $TMP/${DB_NAME}${timestamp}.sql
  fi
  if [ $1 == "restore" ]; then
    testMysql
    mysql --user=$DB_USER --password=$DB_PW --databases $DB_NAME < $TMP/${DB_NAME}
  fi
}

function testMysql {
  UE=$(echo "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = 'pwiki')" | mysql -u root -p -s -N)
  if [ $UE -eq "1" ]; then
    echo "User already setted, going on..."
  fi
}

function files {
  case $1 in
    backup)
      # Create backup of config and other datas
      #FILE2B=${WPATH}'/LocalSettings.php '${WPATH}'/extensions '${WPATH}'/images'
      #echo $FILE2B
      tar -pcvf $TMP/${ARCH}-${timestamp}.tar -C $WPATH .
    ;;
    export)
      tar -pxvf $TMP/*.tar -C $WPATH
    ;;
  esac
}


function archive {
  # Create the tar archive with all of the previous data
  if [ $1 == "create" ]; then
    tar -pcavf $BPATH/$ARCH.tar.gz -C $TMP .
  elif [ $1 == "extract"]; then
    tar -apxf $BPATH/$ARCH -C $TMP
  fi
  echo 'archive done: $BPATH/$ARCH'
}

function cleanall {
# delete unusefull TMP files
  echo "Can I delete $TMP? BE CAREFUL!!! (y/n)"
  read -p " > " REMOVE
  if [ $REMOVE == "y" ]; then
    rm -r $TMP
  fi
}



